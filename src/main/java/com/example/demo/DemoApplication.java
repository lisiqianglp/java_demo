package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.*;
import org.springframework.web.bind.annotation.*;

import java.sql.*;
import java.util.*;

@RestController
@SpringBootApplication
public class DemoApplication {
	// MySQL 8.0 以下版本 - JDBC 驱动名及数据库 URL
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
    static final String DB_URL = "jdbc:mysql://115.28.77.187:3306/test";

    // 数据库的用户名与密码，需要根据自己的设置
    static final String USER = "hbroot";
    static final String PASS = "LXAdhZ9KLaFaevQG";

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@RequestMapping("/hello")
	public String hello()
	{
		
		return "lisiqin";
	}

	@RequestMapping("/test2")
	public String test2() 
	{
		// 数组大小
		int size = 10;
		// 定义数组
		double[] myList = new double[size];
		myList[0] = 5.6;
		myList[1] = 4.5;
		myList[2] = 3.3;
		myList[3] = 13.2;
		myList[4] = 4.0;
		myList[5] = 34.33;
		myList[6] = 34.0;
		myList[7] = 45.45;
		myList[8] = 99.993;
		myList[9] = 11123;
		// 计算所有元素的总和
		double total = 0;
		for (int i = 0; i < size; i++) {
			total += myList[i];
		}
		System.out.println("总和为： " + total);

		return ("总和为： " + total);
   }

	@RequestMapping("/test")
	public List<Map<String, String>> test()
	{
		Connection conn = null;
        Statement stmt = null;

        try{
            // 注册 JDBC 驱动
            Class.forName(JDBC_DRIVER);

            // 打开链接
            System.out.println("连接数据库...");
            conn = DriverManager.getConnection(DB_URL,USER,PASS);

            // 执行查询
            System.out.println(" 实例化Statement对象...");
            stmt = conn.createStatement();
            String sql;
            sql = "SELECT id, name, url FROM websites";

			System.out.print(sql);

            ResultSet rs = stmt.executeQuery(sql);


            List<Map<String, String>> list = new ArrayList<>();
            Map<String, String> map;


            // 展开结果集数据库
            while(rs.next()){
                map = new HashMap<>();
                // 通过字段检索

                map.put("id", rs.getInt("id") + "");
                map.put("name", rs.getString("name"));
                map.put("url", rs.getString("url"));

                list.add(map);


                // 输出数据
                // System.out.print("ID: " + id);
                // System.out.print(", 站点名称: " + name);
                // System.out.print(", 站点 URL: " + url);
                // System.out.print("\n");
            }
            // 完成后关闭
            rs.close();
            stmt.close();
            conn.close();

            return list;
        }catch(SQLException se){
            // 处理 JDBC 错误
            se.printStackTrace();
        }catch(Exception e){
            // 处理 Class.forName 错误
            e.printStackTrace();
        }finally{
            // 关闭资源
            try{
                if(stmt!=null) stmt.close();
            }catch(SQLException se2){
            }// 什么都不做
            try{
                if(conn!=null) conn.close();
            }catch(SQLException se){
                se.printStackTrace();
            }
        }
        System.out.println("Goodbye!");

		return null;
	}

}
